"""
URL configuration for tracker project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/5.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.contrib import admin
from django.urls import path, include
from django.shortcuts import redirect
from accounts.urls import user_login, user_logout, user_signup
from tasks.urls import create_task, show_my_tasks
from projects.urls import show_project, create_project


def redirect_to_home(request):
    response = redirect("/projects/")
    return response


urlpatterns = [
    path("", redirect_to_home, name="home"),
    path("admin/", admin.site.urls),
    path("", include("projects.urls")),
    path("accounts/login/", user_login, name="login"),
    path("accounts/logout/", user_logout, name="logout"),
    path("accounts/signup/", user_signup, name="signup"),
    path("<int:id>/", show_project, name="show_project"),
    path("tasks/create/", create_task, name="create_task"),
    path("projects/create/", create_project, name="create_project"),  # type: ignore
    path("tasks/mine/", show_my_tasks, name="show_my_tasks"),
]
