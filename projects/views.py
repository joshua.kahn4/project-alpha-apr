from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpRequest
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm


# Create your views here.
@login_required
def list_projects(request: HttpRequest):
    projects = Project.objects.filter(owner=request.user)
    context = {"projects": projects}
    return render(request, "projects/list.html", context)


@login_required
def show_project(request: HttpRequest, id: int):
    project: Project = get_object_or_404(Project, id=id)
    return render(request, "projects/detail.html", {"project": project})


@login_required  # type: ignore
def create_project(request: HttpRequest):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save()
            project.assignee = request.user
            project.save()
            return redirect("/projects/")
    else:
        form = ProjectForm()
    context = {"form": form}
    return render(request, "projects/create.html", context)
